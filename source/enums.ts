// Disable indent rule for this file because it is broken (https://github.com/typescript-eslint/typescript-eslint/issues/1824)
/* eslint-disable @typescript-eslint/indent */

/**
 * Enum that denotes the availability of an ElectrumClient.
 * @enum {number}
 * @property {0} UNAVAILABLE   The client is currently not available.
 * @property {1} AVAILABLE     The client is available for use.
 */
export enum ClientState
{
	UNAVAILABLE = 0,
	AVAILABLE = 1,
}

/**
 * Enum that denotes the connection status of an ElectrumConnection.
 * @enum {number}
 * @property {0} DISCONNECTED    The connection is disconnected.
 * @property {1} AVAILABLE       The connection is connected.
 * @property {2} DISCONNECTING   The connection is disconnecting.
 * @property {3} CONNECTING      The connection is connecting.
 * @property {4} RECONNECTING    The connection is restarting.
 */
export enum ConnectionStatus
{
	DISCONNECTED = 0,
	CONNECTED = 1,
	DISCONNECTING = 2,
	CONNECTING = 3,
	RECONNECTING = 4,
}

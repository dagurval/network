import { EventEmitter } from "events";
type RPCParameter = string | number | boolean | null;
interface RPCBase {
    jsonrpc: string;
}
interface RPCNotification extends RPCBase {
    method: string;
    params?: RPCParameter[];
}
interface RPCStatement extends RPCBase {
    id: number | null;
    result: string;
}
interface RPCError {
    code: number;
    message: string;
    data?: any;
}
interface RPCErrorResponse extends RPCBase {
    id: number | null;
    error: RPCError;
}
type RPCResponse = RPCErrorResponse | RPCStatement;
/**
 * Enum that denotes the availability of an ElectrumClient.
 * @enum {number}
 * @property {0} UNAVAILABLE   The client is currently not available.
 * @property {1} AVAILABLE     The client is available for use.
 */
export enum ClientState {
    UNAVAILABLE = 0,
    AVAILABLE = 1
}
/**
 * Enum that denotes the connection status of an ElectrumConnection.
 * @enum {number}
 * @property {0} DISCONNECTED    The connection is disconnected.
 * @property {1} AVAILABLE       The connection is connected.
 * @property {2} DISCONNECTING   The connection is disconnecting.
 * @property {3} CONNECTING      The connection is connecting.
 * @property {4} RECONNECTING    The connection is restarting.
 */
export enum ConnectionStatus {
    DISCONNECTED = 0,
    CONNECTED = 1,
    DISCONNECTING = 2,
    CONNECTING = 3,
    RECONNECTING = 4
}
export interface ClientConfig {
    state: ClientState;
    connection: ElectrumClient;
}
/**
 * A list of possible responses to requests.
 */
export type RequestResponse = object | string | number | boolean | null | RequestResponse[];
export type RequestResolver = (error?: Error, data?: string) => void;
export type ResolveFunction<T> = (value: T | PromiseLike<T>) => void;
export type RejectFunction = (reason?: any) => void;
export interface VersionRejected {
    error: RPCError;
}
export interface VersionNegotiated {
    software: string;
    protocol: string;
}
export const isVersionRejected: (object: any) => object is VersionRejected;
export const isVersionNegotiated: (object: any) => object is VersionNegotiated;
/**
 * Possible Transport Schemes for communication with the Electrum server
 */
export type TransportScheme = 'tcp' | 'tcp_tls' | 'ws' | 'wss';
export interface ConnectionOptions {
    rejectUnauthorized?: boolean;
    serverName?: string;
}
/**
 * Object containing the commonly used ports and schemes for specific Transports.
 * @example const electrum = new ElectrumClient('Electrum client example', '1.4.1', 'bch.imaginary.cash', Transport.WSS.Port, Transport.WSS.Scheme);
 *
 * @property {object} TCP       Port and Scheme to use unencrypted TCP sockets.
 * @property {object} TCP_TLS   Port and Scheme to use TLS-encrypted TCP sockets.
 * @property {object} WS        Port and Scheme to use unencrypted WebSockets.
 * @property {object} WSS       Port and Scheme to use TLS-encrypted WebSockets.
 */
export const ElectrumTransport: {
    TCP: {
        Port: number;
        Scheme: TransportScheme;
    };
    TCP_TLS: {
        Port: number;
        Scheme: TransportScheme;
    };
    WS: {
        Port: number;
        Scheme: TransportScheme;
    };
    WSS: {
        Port: number;
        Scheme: TransportScheme;
    };
};
export const DefaultParameters: {
    PORT: number;
    TRANSPORT_SCHEME: TransportScheme;
    RECONNECT: number;
    TIMEOUT: number;
    PING_INTERVAL: number;
    USE_BIG_INT: boolean;
};
/**
 * Triggers when the underlying connection is established.
 *
 * @event ElectrumClient#connected
 */
/**
 * Triggers when the underlying connection is lost.
 *
 * @event ElectrumClient#disconnected
 */
/**
 * Triggers when the remote server sends data that is not a request response.
 *
 * @event ElectrumClient#notification
 */
/**
 * High-level Electrum client that lets applications send requests and subscribe to notification events from a server.
 */
export class ElectrumClient extends EventEmitter {
    application: string;
    version: string;
    host: string;
    port: number;
    scheme: TransportScheme;
    timeout: number;
    pingInterval: number;
    reconnectInterval: number;
    useBigInt: boolean;
    /**
     * The name and version of the server software indexing the blockchain.
     */
    software: string;
    /**
     * The genesis hash of the blockchain indexed by the server.
     * @note This is only available after a 'server.features' call.
     */
    genesisHash: string;
    /**
     * The chain height of the blockchain indexed by the server.
     * @note This is only available after a 'blockchain.headers.subscribe' call.
     */
    chainHeight: number;
    /**
     * Timestamp of when we last received data from the server indexing the blockchain.
     */
    lastReceivedTimestamp: number;
    /**
     * Number corresponding to the underlying connection status.
     */
    status: ConnectionStatus;
    /**
     * Initializes an Electrum client.
     *
     * @param {string} application            your application name, used to identify to the electrum host.
     * @param {string} version                protocol version to use with the host.
     * @param {string} host                   fully qualified domain name or IP number of the host.
     * @param {number} port                   the TCP network port of the host.
     * @param {TransportScheme} scheme        the transport scheme to use for connection
     * @param {number} timeout                how long network delays we will wait for before taking action, in milliseconds.
     * @param {number} pingInterval           the time between sending pings to the electrum host, in milliseconds.
     * @param {number} reconnectInterval      the time between reconnection attempts to the electrum host, in milliseconds.
     * @param {boolean} useBigInt			  whether to use bigint for numbers in json response.
     *
     * @throws {Error} if `version` is not a valid version string.
     */
    constructor(application: string, version: string, host: string, port?: number, scheme?: TransportScheme, timeout?: number, pingInterval?: number, reconnectInterval?: number, useBigInt?: boolean);
    /**
     * Connects to the remote server.
     *
     * @throws {Error} if the socket connection fails.
     * @returns a promise resolving when the connection is established.
     */
    connect(): Promise<void>;
    /**
     * Disconnects from the remote server and removes all event listeners/subscriptions and open requests.
     *
     * @param {boolean} force                 disconnect even if the connection has not been fully established yet.
     * @param {boolean} retainSubscriptions   retain subscription data so they will be restored on reconnection.
     *
     * @returns true if successfully disconnected, or false if there was no connection.
     */
    disconnect(force?: boolean, retainSubscriptions?: boolean): Promise<boolean>;
    /**
     * Calls a method on the remote server with the supplied parameters.
     *
     * @param {string} method          name of the method to call.
     * @param {...string} parameters   one or more parameters for the method.
     *
     * @throws {Error} if the client is disconnected.
     * @returns a promise that resolves with the result of the method or an Error.
     */
    request(method: string, ...parameters: RPCParameter[]): Promise<Error | RequestResponse>;
    /**
     * Subscribes to the method and payload at the server.
     *
     * @note the response for the subscription request is issued as a notification event.
     *
     * @param {string}    method       one of the subscribable methods the server supports.
     * @param {...string} parameters   one or more parameters for the method.
     *
     * @throws {Error} if the client is disconnected.
     * @returns a promise resolving when the subscription is established.
     */
    subscribe(method: string, ...parameters: RPCParameter[]): Promise<void>;
    /**
     * Unsubscribes to the method at the server and removes any callback functions
     * when there are no more subscriptions for the method.
     *
     * @param {string}    method       a previously subscribed to method.
     * @param {...string} parameters   one or more parameters for the method.
     *
     * @throws {Error} if no subscriptions exist for the combination of the provided `method` and `parameters.
     * @throws {Error} if the client is disconnected.
     * @returns a promise resolving when the subscription is removed.
     */
    unsubscribe(method: string, ...parameters: RPCParameter[]): Promise<void>;
    /**
     * Parser messages from the remote server to resolve request promises and emit subscription events.
     *
     * @param {RPCNotification | RPCResponse} message   the response message
     *
     * @throws {Error} if the message ID does not match an existing request.
     * @ignore
     */
    response(message: RPCNotification | RPCResponse): void;
    /**
     * Callback function that is called when connection to the Electrum server is lost.
     * Aborts all active requests with an error message indicating that connection was lost.
     *
     * @ignore
     */
    onConnectionDisconnect(): Promise<void>;
    /**
     * Stores the server provider software version field on successful version negotiation.
     *
     * @ignore
     */
    storeSoftwareVersion(versionStatement: any): Promise<void>;
    /**
     * Updates the last received timestamp.
     *
     * @ignore
     */
    updateLastReceivedTimestamp(): Promise<void>;
    /**
     * Checks if the provided message is a response to a headers subscription,
     * and if so updates the locally stored chain height value for this client.
     *
     * @ignore
     */
    updateChainHeightFromHeadersNotifications(message: any): Promise<void>;
    /**
     * Checks if the provided message is a response to a server.features request,
     * and if so stores the genesis hash for this client locally.
     *
     * @ignore
     */
    storeGenesisHashFromFeaturesResponse(message: any): Promise<void>;
    /**
     * Helper function to synchronize state and events with the underlying connection.
     */
    handleConnectionStatusChanges(eventName: any): Promise<void>;
}

//# sourceMappingURL=index.d.ts.map

import {EventEmitter as $dvphU$EventEmitter} from "events";
import {Mutex as $dvphU$Mutex} from "async-mutex";
import $dvphU$debug from "debug";
import {parse as $dvphU$parse, parseNumberAndBigInt as $dvphU$parseNumberAndBigInt} from "lossless-json";
import {connect as $dvphU$connect, TLSSocket as $dvphU$TLSSocket} from "tls";
import {isIP as $dvphU$isIP, connect as $dvphU$connect1} from "net";
import {WebSocket as $dvphU$WebSocket} from "@monsterbitar/isomorphic-ws";


function $parcel$export(e, n, v, s) {
  Object.defineProperty(e, n, {get: v, set: s, enumerable: true, configurable: true});
}

// Create the debug logs.
const $623a31cc1663a627$var$debug = {
    client: (0, $dvphU$debug)("electrum-cash:client "),
    errors: (0, $dvphU$debug)("electrum-cash:error  "),
    warning: (0, $dvphU$debug)("electrum-cash:warning"),
    network: (0, $dvphU$debug)("electrum-cash:network"),
    ping: (0, $dvphU$debug)("electrum-cash:pulses ")
};
// Set log colors.
$623a31cc1663a627$var$debug.client.color = "2";
$623a31cc1663a627$var$debug.errors.color = "9";
$623a31cc1663a627$var$debug.warning.color = "13";
$623a31cc1663a627$var$debug.network.color = "4";
$623a31cc1663a627$var$debug.ping.color = "8";
var // Export the logs.
$623a31cc1663a627$export$2e2bcd8739ae039 = $623a31cc1663a627$var$debug;



/**
 * Grouping of utilities that simplifies implementation of the Electrum protocol.
 *
 * @ignore
 */ class $24139611f53a54b8$var$ElectrumProtocol {
    /**
	 * Helper function that builds an Electrum request object.
	 *
	 * @param {string} method       method to call.
	 * @param {array}  parameters   method parameters for the call.
	 * @param {string} requestId    unique string or number referencing this request.
	 *
	 * @returns a properly formatted Electrum request string.
	 */ static buildRequestObject(method, parameters, requestId) {
        // Return the formatted request object.
        // NOTE: Electrum either uses JsonRPC strictly or loosely.
        //       If we specify protocol identifier without being 100% compliant, we risk being disconnected/blacklisted.
        //       For this reason, we omit the protocol identifier to avoid issues.
        return JSON.stringify({
            method: method,
            params: parameters,
            id: requestId
        });
    }
    /**
	 * Constant used to verify if a provided string is a valid version number.
	 *
	 * @returns a regular expression that matches valid version numbers.
	 */ static get versionRegexp() {
        return /^\d+(\.\d+)+$/;
    }
    /**
	 * Constant used to separate statements/messages in a stream of data.
	 *
	 * @returns the delimiter used by Electrum to separate statements.
	 */ static get statementDelimiter() {
        return "\n";
    }
}
var // export the protocol.
$24139611f53a54b8$export$2e2bcd8739ae039 = $24139611f53a54b8$var$ElectrumProtocol;


var $e83d2e7688025acd$exports = {};

$parcel$export($e83d2e7688025acd$exports, "isVersionRejected", () => $e83d2e7688025acd$export$e1f38ab2b4ebdde6);
$parcel$export($e83d2e7688025acd$exports, "isVersionNegotiated", () => $e83d2e7688025acd$export$9598f0c76aa41d73);
const $e83d2e7688025acd$export$e1f38ab2b4ebdde6 = function(object) {
    return "error" in object;
};
const $e83d2e7688025acd$export$9598f0c76aa41d73 = function(object) {
    return "software" in object && "protocol" in object;
};


var $d801b1f9b7fc3074$exports = {};

$parcel$export($d801b1f9b7fc3074$exports, "ElectrumTransport", () => $d801b1f9b7fc3074$export$d048df559e6d3842);
$parcel$export($d801b1f9b7fc3074$exports, "DefaultParameters", () => $d801b1f9b7fc3074$export$f019be48b3aacb1a);
const $d801b1f9b7fc3074$export$d048df559e6d3842 = {
    TCP: {
        Port: 50001,
        Scheme: "tcp"
    },
    TCP_TLS: {
        Port: 50002,
        Scheme: "tcp_tls"
    },
    WS: {
        Port: 50003,
        Scheme: "ws"
    },
    WSS: {
        Port: 50004,
        Scheme: "wss"
    }
};
const $d801b1f9b7fc3074$export$f019be48b3aacb1a = {
    // Port number for TCP TLS connections
    PORT: $d801b1f9b7fc3074$export$d048df559e6d3842.TCP_TLS.Port,
    // Transport to connect to the Electrum server
    TRANSPORT_SCHEME: $d801b1f9b7fc3074$export$d048df559e6d3842.TCP_TLS.Scheme,
    // How long to wait before attempting to reconnect, in milliseconds.
    RECONNECT: 5000,
    // How long to wait for network operations before following up, in milliseconds.
    TIMEOUT: 30000,
    // Time between ping messages, in milliseconds. Pinging keeps the connection alive.
    // The reason for pinging this frequently is to detect connection problems early.
    PING_INTERVAL: 1000,
    // If we use BigInt for numbers in json when parsing and returning json response from the server.
    USE_BIG_INT: false
};









class $0a4cf22b9d6c493c$export$22c0ca2c816c3e08 extends (0, $dvphU$EventEmitter) {
    /**
	 * Connect to host:port using the specified transport
	 *
	 * @param {string} host              Fully qualified domain name or IP address of the host
	 * @param {number} port              Network port for the host to connect to
	 * @param {TransportScheme} scheme   Transport scheme to use
	 * @param {number} timeout           If no connection is established after `timeout` ms, the connection is terminated
	 *
	 * @throws {Error} if an incorrect transport scheme is specified
	 */ connect(host, port, scheme, timeout) {
        // Check that no existing socket exists before initiating a new connection.
        if (this.tcpSocket) throw new Error("Cannot initiate a new socket connection when an existing connection exists");
        // Set a timer to force disconnect after `timeout` seconds
        this.timers.disconnect = setTimeout(()=>this.disconnectOnTimeout(host, port, timeout), timeout);
        // Remove the timer if a connection is successfully established
        this.once("connect", this.clearDisconnectTimerOnTimeout);
        // Define how to refer to the connection scheme in debug output.
        const socketTypes = {
            [(0, $d801b1f9b7fc3074$export$d048df559e6d3842).TCP.Scheme]: "a TCP Socket",
            [(0, $d801b1f9b7fc3074$export$d048df559e6d3842).TCP_TLS.Scheme]: "an encrypted TCP socket"
        };
        // Log that we are trying to establish a connection.
        (0, $623a31cc1663a627$export$2e2bcd8739ae039).network(`Initiating ${socketTypes[scheme]} connection to '${host}:${port}'.`);
        if (scheme !== (0, $d801b1f9b7fc3074$export$d048df559e6d3842).TCP.Scheme && scheme !== (0, $d801b1f9b7fc3074$export$d048df559e6d3842).TCP_TLS.Scheme) // Throw an error if an incorrect transport is specified
        throw new Error("Incorrect transport specified");
        if (scheme === (0, $d801b1f9b7fc3074$export$d048df559e6d3842).TCP_TLS.Scheme) {
            // Initialize connection options.
            const connectionOptions = {
                rejectUnauthorized: false
            };
            // If the hostname is not an IP address..
            if (!$dvphU$isIP(host)) // Set the servername option which enables support for SNI.
            // NOTE: SNI enables a server that hosts multiple domains to provide the appropriate TLS certificate.
            connectionOptions.serverName = host;
            // Initialize the socket (allowing self-signed certificates).
            this.tcpSocket = $dvphU$connect(port, host, connectionOptions);
            // Add a 'secureConnect' listener that checks the authorization status of
            // the socket, and logs a warning when it uses a self signed certificate.
            this.tcpSocket.once("secureConnect", ()=>{
                // Cannot happen, since this event callback *only* exists on TLSSocket
                if (!(this.tcpSocket instanceof $dvphU$TLSSocket)) return;
                // Force cast authorizationError from Error to string (through unknown)
                // because it is incorrectly typed as an Error
                const authorizationError = this.tcpSocket.authorizationError;
                if (authorizationError === "DEPTH_ZERO_SELF_SIGNED_CERT") (0, $623a31cc1663a627$export$2e2bcd8739ae039).warning(`Connection to ${host}:${port} uses a self-signed certificate`);
            });
            // Trigger successful connection events.
            this.tcpSocket.on("secureConnect", this.onConnect.bind(this, socketTypes[scheme], host, port));
        } else {
            // Initialize the socket.
            this.tcpSocket = $dvphU$connect1({
                host: host,
                port: port
            });
            // Trigger successful connection events.
            this.tcpSocket.on("connect", this.onConnect.bind(this, socketTypes[scheme], host, port));
        }
        // Configure encoding.
        this.tcpSocket.setEncoding("utf8");
        // Enable persistent connections.
        // NOTE: This will send a non-data message 0.25 second after last activity.
        //       After 10 consecutive such messages with no response, the connection will be cut.
        this.tcpSocket.setKeepAlive(true, 250);
        // Disable buffering of outgoing data.
        this.tcpSocket.setNoDelay(true);
        // Forward the encountered errors.
        this.tcpSocket.on("error", this.eventForwarders.tcpError);
    }
    /**
	 * Sets up forwarding of events related to the connection.
	 *
	 * @param {string} connectionType   Name of the connection/transport type, used for logging.
	 * @param {string} host             Fully qualified domain name or IP address of the host
	 * @param {number} port             Network port for the host to connect to
	 */ onConnect(connectionType, host, port) {
        // If the onConnect function has already run, do not execute it again.
        if (this.onConnectHasRun) return;
        // Log that the connection has been established.
        (0, $623a31cc1663a627$export$2e2bcd8739ae039).network(`Established ${connectionType} connection with '${host}:${port}'.`);
        // Forward the socket events
        this.tcpSocket.addListener("close", this.eventForwarders.disconnect);
        this.tcpSocket.addListener("data", this.eventForwarders.tcpData);
        // Indicate that the onConnect function has run.
        this.onConnectHasRun = true;
        // Emit the connect event.
        this.emit("connect");
    }
    /**
	 * Clears the disconnect timer if it is still active.
	 */ clearDisconnectTimerOnTimeout() {
        // Clear the retry timer if it is still active.
        if (this.timers.disconnect) clearTimeout(this.timers.disconnect);
    }
    /**
	 * Forcibly terminate the connection.
	 *
	 * @throws {Error} if no connection was found
	 */ disconnect() {
        // Clear the disconnect timer so that the socket does not try to disconnect again later.
        this.clearDisconnectTimerOnTimeout();
        if (this.tcpSocket) {
            // Remove all event forwarders.
            this.tcpSocket.removeListener("close", this.eventForwarders.disconnect);
            this.tcpSocket.removeListener("data", this.eventForwarders.tcpData);
            this.tcpSocket.removeListener("error", this.eventForwarders.tcpError);
            // Terminate the connection.
            this.tcpSocket.destroy();
            // Remove the stored socket.
            this.tcpSocket = undefined;
        }
        // Indicate that the onConnect function has not run and it has to be run again.
        this.onConnectHasRun = false;
        // Emit a disconnect event
        this.emit("disconnect");
    }
    /**
	 * Write data to the socket
	 *
	 * @param {Uint8Array | string} data   Data to be written to the socket
	 * @param {function} callback          Callback function to be called when the write has completed
	 *
	 * @throws {Error} if no connection was found
	 * @returns true if the message was fully flushed to the socket, false if part of the message
	 * is queued in the user memory
	 */ write(data, callback) {
        // Throw an error if no active connection is found
        if (!this.tcpSocket) throw new Error("Cannot write to socket when there is no active connection");
        // Write data to the TLS Socket and return the status indicating
        // whether the full message was flushed to the socket
        return this.tcpSocket.write(data, callback);
    }
    /**
	 * Force a disconnection if no connection is established after `timeout` milliseconds.
	 *
	 * @param {string} host      Host of the connection that timed out
	 * @param {number} port      Port of the connection that timed out
	 * @param {number} timeout   Elapsed milliseconds
	 */ disconnectOnTimeout(host, port, timeout) {
        // Remove the connect listener.
        this.removeListener("connect", this.clearDisconnectTimerOnTimeout);
        // Create a new timeout error.
        const timeoutError = {
            code: "ETIMEDOUT",
            message: `Connection to '${host}:${port}' timed out after ${timeout} milliseconds`
        };
        // Emit an error event so that connect is rejected upstream.
        this.emit("error", timeoutError);
        // Forcibly disconnect to clean up the connection on timeout
        this.disconnect();
    }
    constructor(...args){
        super(...args);
        // Declare timers for keep-alive pings and reconnection
        this.timers = {};
        // Initialize boolean that indicates whether the onConnect function has run (initialize to false).
        this.onConnectHasRun = false;
        // Initialize event forwarding functions.
        this.eventForwarders = {
            disconnect: ()=>this.emit("disconnect"),
            tcpData: (data)=>this.emit("data", data),
            tcpError: (err)=>this.emit("error", err)
        };
    }
}
class $0a4cf22b9d6c493c$export$25b4633f61498e1 extends (0, $dvphU$EventEmitter) {
    /**
	 * Connect to host:port using the specified transport
	 *
	 * @param {string} host              Fully qualified domain name or IP address of the host
	 * @param {number} port              Network port for the host to connect to
	 * @param {TransportScheme} scheme   Transport scheme to use
	 * @param {number} timeout           If no connection is established after `timeout` ms, the connection is terminated
	 *
	 * @throws {Error} if an incorrect transport scheme is specified
	 */ connect(host, port, scheme, timeout) {
        // Check that no existing socket exists before initiating a new connection.
        if (this.webSocket) throw new Error("Cannot initiate a new socket connection when an existing connection exists");
        // Set a timer to force disconnect after `timeout` seconds
        this.timers.disconnect = setTimeout(()=>this.disconnectOnTimeout(host, port, timeout), timeout);
        // Remove the timer if a connection is successfully established
        this.once("connect", this.clearDisconnectTimerOnTimeout);
        // Define how to refer to the connection scheme in debug output.
        const socketTypes = {
            [(0, $d801b1f9b7fc3074$export$d048df559e6d3842).WS.Scheme]: "a WebSocket",
            [(0, $d801b1f9b7fc3074$export$d048df559e6d3842).WSS.Scheme]: "an encrypted WebSocket"
        };
        // Log that we are trying to establish a connection.
        (0, $623a31cc1663a627$export$2e2bcd8739ae039).network(`Initiating ${socketTypes[scheme]} connection to '${host}:${port}'.`);
        if (scheme !== (0, $d801b1f9b7fc3074$export$d048df559e6d3842).WS.Scheme && scheme !== (0, $d801b1f9b7fc3074$export$d048df559e6d3842).WSS.Scheme) // Throw an error if an incorrect transport is specified
        throw new Error("Incorrect transport specified");
        if (scheme === (0, $d801b1f9b7fc3074$export$d048df559e6d3842).WSS.Scheme) // Initialize this.webSocket (rejecting self-signed certificates).
        // We reject self-signed certificates to match functionality of browsers.
        this.webSocket = new (0, $dvphU$WebSocket)(`wss://${host}:${port}`);
        else // Initialize this.webSocket.
        this.webSocket = new (0, $dvphU$WebSocket)(`ws://${host}:${port}`);
        // Trigger successful connection events.
        this.webSocket.addEventListener("open", this.onConnect.bind(this, socketTypes[scheme], host, port));
        // Forward the encountered errors.
        this.webSocket.addEventListener("error", this.eventForwarders.wsError);
    }
    /**
	 * Sets up forwarding of events related to the connection.
	 *
	 * @param {string} connectionType   Name of the connection/transport type, used for logging.
	 * @param {string} host             Fully qualified domain name or IP address of the host
	 * @param {number} port             Network port for the host to connect to
	 */ onConnect(connectionType, host, port) {
        // If the onConnect function has already run, do not execute it again.
        if (this.onConnectHasRun) return;
        // Log that the connection has been established.
        (0, $623a31cc1663a627$export$2e2bcd8739ae039).network(`Established ${connectionType} connection with '${host}:${port}'.`);
        // Forward the socket events
        this.webSocket.addEventListener("close", this.eventForwarders.disconnect);
        this.webSocket.addEventListener("message", this.eventForwarders.wsData);
        // Indicate that the onConnect function has run.
        this.onConnectHasRun = true;
        // Emit the connect event.
        this.emit("connect");
    }
    /**
	 * Clears the disconnect timer if it is still active.
	 */ clearDisconnectTimerOnTimeout() {
        // Clear the retry timer if it is still active.
        if (this.timers.disconnect) clearTimeout(this.timers.disconnect);
    }
    /**
	 * Forcibly terminate the connection.
	 *
	 * @throws {Error} if no connection was found
	 */ disconnect() {
        // Clear the disconnect timer so that the socket does not try to disconnect again later.
        this.clearDisconnectTimerOnTimeout();
        try {
            // Remove all event forwarders.
            this.webSocket.removeEventListener("close", this.eventForwarders.disconnect);
            this.webSocket.removeEventListener("message", this.eventForwarders.wsData);
            this.webSocket.removeEventListener("error", this.eventForwarders.wsError);
            // Gracefully terminate the connection.
            this.webSocket.close();
        } catch (ignored) {
        // close() will throw an error if the connection has not been established yet.
        // We ignore this error, since no similar error gets thrown in the TLS Socket.
        } finally{
            // Remove the stored socket regardless of any thrown errors.
            this.webSocket = undefined;
        }
        // Indicate that the onConnect function has not run and it has to be run again.
        this.onConnectHasRun = false;
        // Emit a disconnect event
        this.emit("disconnect");
    }
    /**
	 * Write data to the socket
	 *
	 * @param {Uint8Array | string} data   Data to be written to the socket
	 * @param {function} callback          Callback function to be called when the write has completed
	 *
	 * @throws {Error} if no connection was found
	 * @returns true if the message was fully flushed to the socket, false if part of the message
	 * is queued in the user memory
	 */ write(data, callback) {
        // Throw an error if no active connection is found
        if (!this.webSocket) throw new Error("Cannot write to socket when there is no active connection");
        // Write data to the WebSocket
        this.webSocket.send(data, callback);
        // WebSockets always fit everything in a single request, so we return true
        return true;
    }
    /**
	 * Force a disconnection if no connection is established after `timeout` milliseconds.
	 *
	 * @param {string} host      Host of the connection that timed out
	 * @param {number} port      Port of the connection that timed out
	 * @param {number} timeout   Elapsed milliseconds
	 */ disconnectOnTimeout(host, port, timeout) {
        // Remove the connect listener.
        this.removeListener("connect", this.clearDisconnectTimerOnTimeout);
        // Create a new timeout error.
        const timeoutError = {
            code: "ETIMEDOUT",
            message: `Connection to '${host}:${port}' timed out after ${timeout} milliseconds`
        };
        // Emit an error event so that connect is rejected upstream.
        this.emit("error", timeoutError);
        // Forcibly disconnect to clean up the connection on timeout
        this.disconnect();
    }
    constructor(...args){
        super(...args);
        // Declare timers for keep-alive pings and reconnection
        this.timers = {};
        // Initialize boolean that indicates whether the onConnect function has run (initialize to false).
        this.onConnectHasRun = false;
        // Initialize event forwarding functions.
        this.eventForwarders = {
            disconnect: ()=>this.emit("disconnect"),
            wsData: (event)=>this.emit("data", `${event.data}\n`),
            wsError: (event)=>this.emit("error", event.error)
        };
    }
}


var $db7c797e63383364$exports = {};

$parcel$export($db7c797e63383364$exports, "ClientState", () => $db7c797e63383364$export$c4f81c6d30ca200f);
$parcel$export($db7c797e63383364$exports, "ConnectionStatus", () => $db7c797e63383364$export$7516420eb880ab68);
// Disable indent rule for this file because it is broken (https://github.com/typescript-eslint/typescript-eslint/issues/1824)
/* eslint-disable @typescript-eslint/indent */ /**
 * Enum that denotes the availability of an ElectrumClient.
 * @enum {number}
 * @property {0} UNAVAILABLE   The client is currently not available.
 * @property {1} AVAILABLE     The client is available for use.
 */ var $db7c797e63383364$export$c4f81c6d30ca200f;
(function(ClientState) {
    ClientState[ClientState["UNAVAILABLE"] = 0] = "UNAVAILABLE";
    ClientState[ClientState["AVAILABLE"] = 1] = "AVAILABLE";
})($db7c797e63383364$export$c4f81c6d30ca200f || ($db7c797e63383364$export$c4f81c6d30ca200f = {}));
var $db7c797e63383364$export$7516420eb880ab68;
(function(ConnectionStatus) {
    ConnectionStatus[ConnectionStatus["DISCONNECTED"] = 0] = "DISCONNECTED";
    ConnectionStatus[ConnectionStatus["CONNECTED"] = 1] = "CONNECTED";
    ConnectionStatus[ConnectionStatus["DISCONNECTING"] = 2] = "DISCONNECTING";
    ConnectionStatus[ConnectionStatus["CONNECTING"] = 3] = "CONNECTING";
    ConnectionStatus[ConnectionStatus["RECONNECTING"] = 4] = "RECONNECTING";
})($db7c797e63383364$export$7516420eb880ab68 || ($db7c797e63383364$export$7516420eb880ab68 = {}));



/**
 * Wrapper around TLS/WSS sockets that gracefully separates a network stream into Electrum protocol messages.
 */ class $ff134c9a9e1f7361$var$ElectrumConnection extends (0, $dvphU$EventEmitter) {
    /**
	 * Sets up network configuration for an Electrum client connection.
	 *
	 * @param {string} application            your application name, used to identify to the electrum host.
	 * @param {string} version                protocol version to use with the host.
	 * @param {string} host                   fully qualified domain name or IP number of the host.
	 * @param {number} port                   the network port of the host.
	 * @param {TransportScheme} scheme        the transport scheme to use for connection
	 * @param {number} timeout                how long network delays we will wait for before taking action, in milliseconds.
	 * @param {number} pingInterval           the time between sending pings to the electrum host, in milliseconds.
	 * @param {number} reconnectInterval      the time between reconnection attempts to the electrum host, in milliseconds.
	 * @param {boolean} useBigInt	  		  whether to use bigint for numbers in json response.
	 *
	 * @throws {Error} if `version` is not a valid version string.
	 */ constructor(application, version, host, port = (0, $d801b1f9b7fc3074$export$f019be48b3aacb1a).PORT, scheme = (0, $d801b1f9b7fc3074$export$f019be48b3aacb1a).TRANSPORT_SCHEME, timeout = (0, $d801b1f9b7fc3074$export$f019be48b3aacb1a).TIMEOUT, pingInterval = (0, $d801b1f9b7fc3074$export$f019be48b3aacb1a).PING_INTERVAL, reconnectInterval = (0, $d801b1f9b7fc3074$export$f019be48b3aacb1a).RECONNECT, useBigInt = (0, $d801b1f9b7fc3074$export$f019be48b3aacb1a).USE_BIG_INT){
        // Initialize the event emitter.
        super();
        this.application = application;
        this.version = version;
        this.host = host;
        this.port = port;
        this.scheme = scheme;
        this.timeout = timeout;
        this.pingInterval = pingInterval;
        this.reconnectInterval = reconnectInterval;
        this.useBigInt = useBigInt;
        this.status = (0, $db7c797e63383364$export$7516420eb880ab68).DISCONNECTED;
        this.timers = {};
        this.verifications = [];
        this.messageBuffer = "";
        // Check if the provided version is a valid version number.
        if (!(0, $24139611f53a54b8$export$2e2bcd8739ae039).versionRegexp.test(version)) // Throw an error since the version number was not valid.
        throw new Error(`Provided version string (${version}) is not a valid protocol version number.`);
        // Create an initial network socket.
        this.createSocket();
        // Handle visibility changes when run in a browser environment.
        if (typeof document !== "undefined") document.addEventListener("visibilitychange", this.handleVisibilityChange.bind(this));
        // Handle network connection changes when run in a browser environment.
        if (typeof window !== "undefined") {
            window.addEventListener("online", this.handleNetworkChange.bind(this));
            window.addEventListener("offline", this.handleNetworkChange.bind(this));
        }
    }
    /**
	 * Returns a string for the host identifier for usage in debug messages.
	 */ get hostIdentifier() {
        return `${this.host}:${this.port}`;
    }
    /**
	 * Create and configures a fresh socket and attaches all relevant listeners.
	 */ createSocket() {
        if (this.scheme === (0, $d801b1f9b7fc3074$export$d048df559e6d3842).TCP.Scheme || this.scheme === (0, $d801b1f9b7fc3074$export$d048df559e6d3842).TCP_TLS.Scheme) // Initialize a new ElectrumTcpSocket
        this.socket = new (0, $0a4cf22b9d6c493c$export$22c0ca2c816c3e08)();
        else if (this.scheme === (0, $d801b1f9b7fc3074$export$d048df559e6d3842).WS.Scheme || this.scheme === (0, $d801b1f9b7fc3074$export$d048df559e6d3842).WSS.Scheme) // Initialize a new ElectrumWebSocket
        this.socket = new (0, $0a4cf22b9d6c493c$export$25b4633f61498e1)();
        else // Throw an error if an incorrect transport is specified
        throw new Error(`Provided transport (${this.scheme}) is not a valid ElectrumTransport`);
        // Set up handlers for connection and disconnection.
        this.socket.on("connect", this.onSocketConnect.bind(this));
        this.socket.on("disconnect", this.onSocketDisconnect.bind(this));
        // Set up handler for incoming data.
        this.socket.on("data", this.parseMessageChunk.bind(this));
    }
    /**
	 * Shuts down and destroys the current socket.
	 */ destroySocket() {
        // Close the socket connection and destroy the socket.
        this.socket.disconnect();
    }
    /**
	 * Assembles incoming data into statements and hands them off to the message parser.
	 *
	 * @param {string} data   data to append to the current message buffer, as a string.
	 *
	 * @throws {SyntaxError} if the passed statement parts are not valid JSON.
	 */ parseMessageChunk(data) {
        // Update the timestamp for when we last received data.
        this.lastReceivedTimestamp = Date.now();
        // Emit a notification indicating that the connection has received data.
        this.emit("received");
        // Clear and remove all verification timers.
        this.verifications.forEach((timer)=>clearTimeout(timer));
        this.verifications.length = 0;
        // Add the message to the current message buffer.
        this.messageBuffer += data;
        // Check if the new message buffer contains the statement delimiter.
        while(this.messageBuffer.includes((0, $24139611f53a54b8$export$2e2bcd8739ae039).statementDelimiter)){
            // Split message buffer into statements.
            const statementParts = this.messageBuffer.split((0, $24139611f53a54b8$export$2e2bcd8739ae039).statementDelimiter);
            // For as long as we still have statements to parse..
            while(statementParts.length > 1){
                // Move the first statement to its own variable.
                const currentStatementList = String(statementParts.shift());
                // Parse the statement into an object or list of objects.
                let statementList = (0, $dvphU$parse)(currentStatementList, null, this.useBigInt ? (0, $dvphU$parseNumberAndBigInt) : parseFloat);
                // Wrap the statement in an array if it is not already a batched statement list.
                if (!Array.isArray(statementList)) statementList = [
                    statementList
                ];
                // For as long as there is statements in the result set..
                while(statementList.length > 0){
                    // Move the first statement from the batch to its own variable.
                    const currentStatement = statementList.shift();
                    // If the current statement is a version negotiation response..
                    if (currentStatement.id === "versionNegotiation") {
                        if (currentStatement.error) // Then emit a failed version negotiation response signal.
                        this.emit("version", {
                            error: currentStatement.error
                        });
                        else {
                            // Extract the software and protocol version reported.
                            const [software, protocol] = currentStatement.result;
                            // Emit a successful version negotiation response signal.
                            this.emit("version", {
                                software: software,
                                protocol: protocol
                            });
                        }
                        continue;
                    }
                    // If the current statement is a keep-alive response..
                    if (currentStatement.id === "keepAlive") continue;
                    // Emit the statements for handling higher up in the stack.
                    this.emit("statement", currentStatement);
                }
            }
            // Store the remaining statement as the current message buffer.
            this.messageBuffer = statementParts.shift() || "";
        }
    }
    /**
	 * Sends a keep-alive message to the host.
	 *
	 * @returns true if the ping message was fully flushed to the socket, false if
	 * part of the message is queued in the user memory
	 */ ping() {
        // Write a log message.
        (0, $623a31cc1663a627$export$2e2bcd8739ae039).ping(`Sending keep-alive ping to '${this.hostIdentifier}'`);
        // Craft a keep-alive message.
        const message = (0, $24139611f53a54b8$export$2e2bcd8739ae039).buildRequestObject("server.ping", [], "keepAlive");
        // Send the keep-alive message.
        const status = this.send(message);
        // Return the ping status.
        return status;
    }
    /**
	 * Initiates the network connection negotiates a protocol version. Also emits the 'connect' signal if successful.
	 *
	 * @throws {Error} if the socket connection fails.
	 * @returns a promise resolving when the connection is established
	 */ async connect() {
        // If we are already connected return true.
        if (this.status === (0, $db7c797e63383364$export$7516420eb880ab68).CONNECTED) return;
        // Indicate that the connection is connecting
        this.status = (0, $db7c797e63383364$export$7516420eb880ab68).CONNECTING;
        // Emit a connect event now that the connection is being set up.
        this.emit("connecting");
        // Define a function to wrap connection as a promise.
        const connectionResolver = (resolve, reject)=>{
            const rejector = (error)=>{
                // Set the status back to disconnected
                this.status = (0, $db7c797e63383364$export$7516420eb880ab68).DISCONNECTED;
                // Emit a connect event indicating that we failed to connect.
                this.emit("disconnected");
                // Reject with the error as reason
                reject(error);
            };
            // Replace previous error handlers to reject the promise on failure.
            this.socket.removeAllListeners("error");
            this.socket.once("error", rejector);
            // Define a function to wrap version negotiation as a callback.
            const versionNegotiator = ()=>{
                // Write a log message to show that we have started version negotiation.
                (0, $623a31cc1663a627$export$2e2bcd8739ae039).network(`Requesting protocol version ${this.version} with '${this.hostIdentifier}'.`);
                // remove the one-time error handler since no error was detected.
                this.socket.removeListener("error", rejector);
                // Build a version negotiation message.
                const versionMessage = (0, $24139611f53a54b8$export$2e2bcd8739ae039).buildRequestObject("server.version", [
                    this.application,
                    this.version
                ], "versionNegotiation");
                // Define a function to wrap version validation as a function.
                const versionValidator = (version)=>{
                    // Check if version negotiation failed.
                    if ((0, $e83d2e7688025acd$export$e1f38ab2b4ebdde6)(version)) {
                        // Disconnect from the host.
                        this.disconnect(true);
                        // Declare an error message.
                        const errorMessage = "unsupported protocol version.";
                        // Log the error.
                        (0, $623a31cc1663a627$export$2e2bcd8739ae039).errors(`Failed to connect with ${this.hostIdentifier} due to ${errorMessage}`);
                        // Reject the connection with false since version negotiation failed.
                        reject(errorMessage);
                    } else if (version.protocol !== this.version && `${version.protocol}.0` !== this.version && `${version.protocol}.0.0` !== this.version) {
                        // Disconnect from the host.
                        this.disconnect(true);
                        // Declare an error message.
                        const errorMessage = `incompatible protocol version negotiated (${version.protocol} !== ${this.version}).`;
                        // Log the error.
                        (0, $623a31cc1663a627$export$2e2bcd8739ae039).errors(`Failed to connect with ${this.hostIdentifier} due to ${errorMessage}`);
                        // Reject the connection with false since version negotiation failed.
                        reject(errorMessage);
                    } else {
                        // Write a log message.
                        (0, $623a31cc1663a627$export$2e2bcd8739ae039).network(`Negotiated protocol version ${version.protocol} with '${this.hostIdentifier}', powered by ${version.software}.`);
                        // Set connection status to connected
                        this.status = (0, $db7c797e63383364$export$7516420eb880ab68).CONNECTED;
                        // Emit a connect event now that the connection is usable.
                        this.emit("connected");
                        // Resolve the connection promise since we successfully connected and negotiated protocol version.
                        resolve();
                    }
                };
                // Listen for version negotiation once.
                this.once("version", versionValidator);
                // Send the version negotiation message.
                this.send(versionMessage);
            };
            // Prepare the version negotiation.
            this.socket.once("connect", versionNegotiator);
            // Set up handler for network errors.
            this.socket.on("error", this.onSocketError.bind(this));
            // Connect to the server.
            this.socket.connect(this.host, this.port, this.scheme, this.timeout);
        };
        // Wait until connection is established and version negotiation succeeds.
        await new Promise(connectionResolver);
    }
    /**
	 * Restores the network connection.
	 */ async reconnect() {
        // If a reconnect timer is set, remove it
        await this.clearReconnectTimer();
        // Write a log message.
        (0, $623a31cc1663a627$export$2e2bcd8739ae039).network(`Trying to reconnect to '${this.hostIdentifier}'..`);
        // Set the status to reconnecting for more accurate log messages.
        this.status = (0, $db7c797e63383364$export$7516420eb880ab68).RECONNECTING;
        // Emit a connect event now that the connection is usable.
        this.emit("reconnecting");
        // Destroy and recreate the socket to get a clean slate.
        this.destroySocket();
        this.createSocket();
        try {
            // Try to connect again.
            await this.connect();
        } catch (error) {
        // Do nothing as the error should be handled via the disconnect and error signals.
        }
    }
    /**
	 * Removes the current reconnect timer.
	 */ clearReconnectTimer() {
        // If a reconnect timer is set, remove it
        if (this.timers.reconnect) clearTimeout(this.timers.reconnect);
        // Reset the timer reference.
        this.timers.reconnect = undefined;
    }
    /**
	 * Removes the current keep-alive timer.
	 */ clearKeepAliveTimer() {
        // If a keep-alive timer is set, remove it
        if (this.timers.keepAlive) clearTimeout(this.timers.keepAlive);
        // Reset the timer reference.
        this.timers.keepAlive = undefined;
    }
    /**
	 * Initializes the keep alive timer loop.
	 */ setupKeepAliveTimer() {
        // If the keep-alive timer loop is not currently set up..
        if (!this.timers.keepAlive) // Set a new keep-alive timer.
        this.timers.keepAlive = setTimeout(this.ping.bind(this), this.pingInterval);
    }
    /**
	 * Tears down the current connection and removes all event listeners on disconnect.
	 *
	 * @param {boolean} force         disconnect even if the connection has not been fully established yet.
	 * @param {boolean} intentional   update connection state if disconnect is intentional.
	 *
	 * @returns true if successfully disconnected, or false if there was no connection.
	 */ async disconnect(force = false, intentional = true) {
        // Return early when there is nothing to disconnect from
        if (this.status === (0, $db7c797e63383364$export$7516420eb880ab68).DISCONNECTED && !force) // Return false to indicate that there was nothing to disconnect from.
        return false;
        // Update connection state if the disconnection is intentional.
        // NOTE: The state is meant to represent what the client is requesting, but
        //       is used internally to handle visibility changes in browsers to ensure functional reconnection.
        if (intentional) // Set connection status to null to indicate tear-down is currently happening.
        this.status = (0, $db7c797e63383364$export$7516420eb880ab68).DISCONNECTING;
        // Emit a connect event to indicate that we are disconnecting.
        this.emit("disconnecting");
        // If a keep-alive timer is set, remove it.
        await this.clearKeepAliveTimer();
        // If a reconnect timer is set, remove it
        await this.clearReconnectTimer();
        const disconnectResolver = (resolve)=>{
            // Resolve to true after the connection emits a disconnect
            this.once("disconnected", ()=>resolve(true));
            // Close the connection and destroy the socket.
            this.destroySocket();
        };
        // Return true to indicate that we disconnected.
        return new Promise(disconnectResolver);
    }
    /**
	 * Updates the connection state based on browser reported connectivity.
	 *
	 * Most modern browsers are able to provide information on the connection state
	 * which allows for significantly faster response times to network changes compared
	 * to waiting for network requests to fail.
	 *
	 * When available, we make use of this to fail early to provide a better user experience.
	 */ async handleNetworkChange() {
        // Do nothing if we do not have the navigator available.
        if (typeof window.navigator === "undefined") return;
        // Attempt to reconnect to the network now that we may be online again.
        if (window.navigator.onLine === true) this.reconnect();
        // Disconnected from the network so that cleanup can happen while we're offline.
        if (window.navigator.onLine !== true) {
            const forceDisconnect = true;
            const isUnintended = false;
            this.disconnect(forceDisconnect, isUnintended);
        }
    }
    /**
	 * Updates connection state based on application visibility.
	 *
	 * Some browsers will disconnect network connections when the browser is out of focus,
	 * which would normally cause our reconnect-on-timeout routines to trigger, but that
	 * results in a poor user experience since the events are not handled consistently
	 * and sometimes it can take some time after restoring focus to the browser.
	 *
	 * By manually disconnecting when this happens we prevent the default reconnection routines
	 * and make the behavior consistent across browsers.
	 */ async handleVisibilityChange() {
        // Disconnect when application is removed from focus.
        if (document.visibilityState === "hidden") {
            const forceDisconnect = true;
            const isUnintended = false;
            this.disconnect(forceDisconnect, isUnintended);
        }
        // Reconnect when application is returned to focus.
        if (document.visibilityState === "visible") this.reconnect();
    }
    /**
	 * Sends an arbitrary message to the server.
	 *
	 * @param {string} message   json encoded request object to send to the server, as a string.
	 *
	 * @returns true if the message was fully flushed to the socket, false if part of the message
	 * is queued in the user memory
	 */ send(message) {
        // Remove the current keep-alive timer if it exists.
        this.clearKeepAliveTimer();
        // Get the current timestamp in milliseconds.
        const currentTime = Date.now();
        // Follow up and verify that the message got sent..
        const verificationTimer = setTimeout(this.verifySend.bind(this, currentTime), this.timeout);
        // Store the verification timer locally so that it can be cleared when data has been received.
        this.verifications.push(verificationTimer);
        // Set a new keep-alive timer.
        this.setupKeepAliveTimer();
        // Write the message to the network socket.
        return this.socket.write(message + (0, $24139611f53a54b8$export$2e2bcd8739ae039).statementDelimiter);
    }
    // --- Event managers. --- //
    /**
	 * Marks the connection as timed out and schedules reconnection if we have not
	 * received data within the expected time frame.
	 */ verifySend(sentTimestamp) {
        // If we haven't received any data since we last sent data out..
        if (Number(this.lastReceivedTimestamp) < sentTimestamp) {
            // If this connection is already disconnected, we do not change anything
            if (this.status === (0, $db7c797e63383364$export$7516420eb880ab68).DISCONNECTED || this.status === (0, $db7c797e63383364$export$7516420eb880ab68).DISCONNECTING) // debug.warning(`Tried to verify already disconnected connection to '${this.hostIdentifier}'`);
            return;
            // Remove the current keep-alive timer if it exists.
            this.clearKeepAliveTimer();
            // Write a notification to the logs.
            (0, $623a31cc1663a627$export$2e2bcd8739ae039).network(`Connection to '${this.hostIdentifier}' timed out.`);
            // Close the connection to avoid re-use.
            // NOTE: This initiates reconnection routines if the connection has not
            //       been marked as intentionally disconnected.
            this.socket.disconnect();
        }
    }
    /**
	 * Updates the connection status when a connection is confirmed.
	 */ onSocketConnect() {
        // If a reconnect timer is set, remove it.
        this.clearReconnectTimer();
        // Set up the initial timestamp for when we last received data from the server.
        this.lastReceivedTimestamp = Date.now();
        // Set up the initial keep-alive timer.
        this.setupKeepAliveTimer();
        // Clear all temporary error listeners.
        this.socket.removeAllListeners("error");
        // Set up handler for network errors.
        this.socket.on("error", this.onSocketError.bind(this));
    }
    /**
	 * Updates the connection status when a connection is ended.
	 */ onSocketDisconnect() {
        // Remove the current keep-alive timer if it exists.
        this.clearKeepAliveTimer();
        // If this is a connection we're trying to tear down..
        if (this.status === (0, $db7c797e63383364$export$7516420eb880ab68).DISCONNECTING) {
            // Mark the connection as disconnected.
            this.status = (0, $db7c797e63383364$export$7516420eb880ab68).DISCONNECTED;
            // Send a disconnect signal higher up the stack.
            this.emit("disconnected");
            // If a reconnect timer is set, remove it.
            this.clearReconnectTimer();
            // Remove all event listeners
            this.removeAllListeners();
            // Write a log message.
            (0, $623a31cc1663a627$export$2e2bcd8739ae039).network(`Disconnected from '${this.hostIdentifier}'.`);
        } else {
            // If this is for an established connection..
            if (this.status === (0, $db7c797e63383364$export$7516420eb880ab68).CONNECTED) // Write a notification to the logs.
            (0, $623a31cc1663a627$export$2e2bcd8739ae039).errors(`Connection with '${this.hostIdentifier}' was closed, trying to reconnect in ${this.reconnectInterval / 1000} seconds.`);
            // Mark the connection as disconnected for now..
            this.status = (0, $db7c797e63383364$export$7516420eb880ab68).DISCONNECTED;
            // Send a disconnect signal higher up the stack.
            this.emit("disconnected");
            // If we don't have a pending reconnection timer..
            if (!this.timers.reconnect) // Attempt to reconnect after one keep-alive duration.
            this.timers.reconnect = setTimeout(this.reconnect.bind(this), this.reconnectInterval);
        }
    }
    /**
	 * Notify administrator of any unexpected errors.
	 */ onSocketError(error) {
        // Report a generic error if no error information is present.
        // NOTE: When using WSS, the error event explicitly
        //       only allows to send a "simple" event without data.
        //       https://stackoverflow.com/a/18804298
        if (typeof error === "undefined") // Do nothing, and instead rely on the socket disconnect event for further information.
        return;
        // If the DNS lookup failed.
        if (error.code === "EAI_AGAIN") {
            (0, $623a31cc1663a627$export$2e2bcd8739ae039).errors(`Failed to look up DNS records for '${this.host}'.`);
            return;
        }
        // If the connection timed out..
        if (error.code === "ETIMEDOUT") {
            // Log the provided timeout message.
            (0, $623a31cc1663a627$export$2e2bcd8739ae039).errors(error.message);
            return;
        }
        // Log unknown error
        (0, $623a31cc1663a627$export$2e2bcd8739ae039).errors(`Unknown network error ('${this.hostIdentifier}'): `, error);
    }
}
var // Export the connection.
$ff134c9a9e1f7361$export$2e2bcd8739ae039 = $ff134c9a9e1f7361$var$ElectrumConnection;






// Acceptable parameter types for RPC messages
const $abcb763a48577a1e$export$d73a2e87a509880 = function(message) {
    return "id" in message && "error" in message;
};
const $abcb763a48577a1e$export$81276773828ff315 = function(message) {
    return "id" in message && "result" in message;
};
const $abcb763a48577a1e$export$280de919a0cf6928 = function(message) {
    return !("id" in message) && "method" in message;
};
const $abcb763a48577a1e$export$94e3360fcddccc76 = function(message) {
    return "id" in message && "method" in message;
};



/**
 * Triggers when the underlying connection is established.
 *
 * @event ElectrumClient#connected
 */ /**
 * Triggers when the underlying connection is lost.
 *
 * @event ElectrumClient#disconnected
 */ /**
 * Triggers when the remote server sends data that is not a request response.
 *
 * @event ElectrumClient#notification
 */ /**
 * High-level Electrum client that lets applications send requests and subscribe to notification events from a server.
 */ class $558b46d3f899ced5$var$ElectrumClient extends (0, $dvphU$EventEmitter) {
    /**
	 * Initializes an Electrum client.
	 *
	 * @param {string} application            your application name, used to identify to the electrum host.
	 * @param {string} version                protocol version to use with the host.
	 * @param {string} host                   fully qualified domain name or IP number of the host.
	 * @param {number} port                   the TCP network port of the host.
	 * @param {TransportScheme} scheme        the transport scheme to use for connection
	 * @param {number} timeout                how long network delays we will wait for before taking action, in milliseconds.
	 * @param {number} pingInterval           the time between sending pings to the electrum host, in milliseconds.
	 * @param {number} reconnectInterval      the time between reconnection attempts to the electrum host, in milliseconds.
	 * @param {boolean} useBigInt			  whether to use bigint for numbers in json response.
	 *
	 * @throws {Error} if `version` is not a valid version string.
	 */ constructor(application, version, host, port = (0, $d801b1f9b7fc3074$export$f019be48b3aacb1a).PORT, scheme = (0, $d801b1f9b7fc3074$export$f019be48b3aacb1a).TRANSPORT_SCHEME, timeout = (0, $d801b1f9b7fc3074$export$f019be48b3aacb1a).TIMEOUT, pingInterval = (0, $d801b1f9b7fc3074$export$f019be48b3aacb1a).PING_INTERVAL, reconnectInterval = (0, $d801b1f9b7fc3074$export$f019be48b3aacb1a).RECONNECT, useBigInt = (0, $d801b1f9b7fc3074$export$f019be48b3aacb1a).USE_BIG_INT){
        // Initialize the event emitter.
        super();
        this.application = application;
        this.version = version;
        this.host = host;
        this.port = port;
        this.scheme = scheme;
        this.timeout = timeout;
        this.pingInterval = pingInterval;
        this.reconnectInterval = reconnectInterval;
        this.useBigInt = useBigInt;
        this.status = (0, $db7c797e63383364$export$7516420eb880ab68).DISCONNECTED;
        this.subscriptionMethods = {};
        this.requestId = 0;
        this.requestResolvers = {};
        this.connectionLock = new (0, $dvphU$Mutex)();
        // Set up a connection to an electrum server.
        this.connection = new (0, $ff134c9a9e1f7361$export$2e2bcd8739ae039)(application, version, host, port, scheme, timeout, pingInterval, reconnectInterval, useBigInt);
    }
    /**
	 * Connects to the remote server.
	 *
	 * @throws {Error} if the socket connection fails.
	 * @returns a promise resolving when the connection is established.
	 */ async connect() {
        // Create a lock so that multiple connects/disconnects cannot race each other.
        const unlock = await this.connectionLock.acquire();
        try {
            // If we are already connected, do not attempt to connect again.
            if (this.connection.status === (0, $db7c797e63383364$export$7516420eb880ab68).CONNECTED) return;
            // Listen for parsed statements.
            this.connection.on("statement", this.response.bind(this));
            // Hook up handles for the connected and disconnected events.
            this.connection.on("connected", this.resubscribeOnConnect.bind(this));
            this.connection.on("disconnected", this.onConnectionDisconnect.bind(this));
            // Relay connecting and reconnecting events.
            this.connection.on("connecting", this.handleConnectionStatusChanges.bind(this, "connecting"));
            this.connection.on("disconnecting", this.handleConnectionStatusChanges.bind(this, "disconnecting"));
            this.connection.on("reconnecting", this.handleConnectionStatusChanges.bind(this, "reconnecting"));
            // Hook up client metadata gathering functions.
            this.connection.on("version", this.storeSoftwareVersion.bind(this));
            this.connection.on("received", this.updateLastReceivedTimestamp.bind(this));
            // Relay error events.
            this.connection.on("error", this.emit.bind(this, "error"));
            // Connect with the server.
            await this.connection.connect();
        } finally{
            unlock();
        }
    }
    /**
	 * Disconnects from the remote server and removes all event listeners/subscriptions and open requests.
	 *
	 * @param {boolean} force                 disconnect even if the connection has not been fully established yet.
	 * @param {boolean} retainSubscriptions   retain subscription data so they will be restored on reconnection.
	 *
	 * @returns true if successfully disconnected, or false if there was no connection.
	 */ async disconnect(force = false, retainSubscriptions = false) {
        if (!retainSubscriptions) {
            // Cancel all event listeners.
            this.removeAllListeners();
            // Remove all subscription data
            this.subscriptionMethods = {};
        }
        // Disconnect from the remote server.
        return this.connection.disconnect(force);
    }
    /**
	 * Calls a method on the remote server with the supplied parameters.
	 *
	 * @param {string} method          name of the method to call.
	 * @param {...string} parameters   one or more parameters for the method.
	 *
	 * @throws {Error} if the client is disconnected.
	 * @returns a promise that resolves with the result of the method or an Error.
	 */ async request(method, ...parameters) {
        // If we are not connected to a server..
        if (this.connection.status !== (0, $db7c797e63383364$export$7516420eb880ab68).CONNECTED) // Reject the request with a disconnected error message.
        throw new Error(`Unable to send request to a disconnected server '${this.connection.host}'.`);
        // Increase the request ID by one.
        this.requestId += 1;
        // Store a copy of the request id.
        const id = this.requestId;
        // Format the arguments as an electrum request object.
        const message = (0, $24139611f53a54b8$export$2e2bcd8739ae039).buildRequestObject(method, parameters, id);
        // Define a function to wrap the request in a promise.
        const requestResolver = (resolve)=>{
            // Add a request resolver for this promise to the list of requests.
            this.requestResolvers[id] = (error, data)=>{
                // If the resolution failed..
                if (error) // Resolve the promise with the error for the application to handle.
                resolve(error);
                else // Resolve the promise with the request results.
                resolve(data);
            };
            // Send the request message to the remote server.
            this.connection.send(message);
        };
        // Write a log message.
        (0, $623a31cc1663a627$export$2e2bcd8739ae039).network(`Sending request '${method}' to '${this.connection.host}'`);
        // return a promise to deliver results later.
        return new Promise(requestResolver);
    }
    /**
	 * Subscribes to the method and payload at the server.
	 *
	 * @note the response for the subscription request is issued as a notification event.
	 *
	 * @param {string}    method       one of the subscribable methods the server supports.
	 * @param {...string} parameters   one or more parameters for the method.
	 *
	 * @throws {Error} if the client is disconnected.
	 * @returns a promise resolving when the subscription is established.
	 */ async subscribe(method, ...parameters) {
        // Initialize an empty list of subscription payloads, if needed.
        if (!this.subscriptionMethods[method]) this.subscriptionMethods[method] = new Set();
        // Store the subscription parameters to track what data we have subscribed to.
        this.subscriptionMethods[method].add(JSON.stringify(parameters));
        // Send initial subscription request.
        const requestData = await this.request(method, ...parameters);
        // Construct a notification structure to package the initial result as a notification.
        const notification = {
            jsonrpc: "2.0",
            method: method,
            params: [
                ...parameters,
                requestData
            ]
        };
        // Manually emit an event for the initial response.
        this.emit("notification", notification);
        // Try to update the chain height.
        this.updateChainHeightFromHeadersNotifications(notification);
    }
    /**
	 * Unsubscribes to the method at the server and removes any callback functions
	 * when there are no more subscriptions for the method.
	 *
	 * @param {string}    method       a previously subscribed to method.
	 * @param {...string} parameters   one or more parameters for the method.
	 *
	 * @throws {Error} if no subscriptions exist for the combination of the provided `method` and `parameters.
	 * @throws {Error} if the client is disconnected.
	 * @returns a promise resolving when the subscription is removed.
	 */ async unsubscribe(method, ...parameters) {
        // Throw an error if the client is disconnected.
        if (this.connection.status !== (0, $db7c797e63383364$export$7516420eb880ab68).CONNECTED) throw new Error(`Unable to send unsubscribe request to a disconnected server '${this.connection.host}'.`);
        // If this method has no subscriptions..
        if (!this.subscriptionMethods[method]) // Reject this promise with an explanation.
        throw new Error(`Cannot unsubscribe from '${method}' since the method has no subscriptions.`);
        // Pack up the parameters as a long string.
        const subscriptionParameters = JSON.stringify(parameters);
        // If the method payload could not be located..
        if (!this.subscriptionMethods[method].has(subscriptionParameters)) // Reject this promise with an explanation.
        throw new Error(`Cannot unsubscribe from '${method}' since it has no subscription with the given parameters.`);
        // Remove this specific subscription payload from internal tracking.
        this.subscriptionMethods[method].delete(subscriptionParameters);
        // Send unsubscription request to the server
        // NOTE: As a convenience we allow users to define the method as the subscribe or unsubscribe version.
        await this.request(method.replace(".subscribe", ".unsubscribe"), ...parameters);
        // Write a log message.
        (0, $623a31cc1663a627$export$2e2bcd8739ae039).client(`Unsubscribed from '${String(method)}' for the '${subscriptionParameters}' parameters.`);
    }
    /**
	 * Restores existing subscriptions without updating status or triggering manual callbacks.
	 *
	 * @throws {Error} if subscription data cannot be found for all stored event names.
	 * @throws {Error} if the client is disconnected.
	 * @returns a promise resolving to true when the subscriptions are restored.
	 *
	 * @ignore
	 */ async resubscribeOnConnect() {
        // Write a log message.
        (0, $623a31cc1663a627$export$2e2bcd8739ae039).client(`Connected to '${this.connection.hostIdentifier}'.`);
        // Synchronize with the underlying connection status.
        this.handleConnectionStatusChanges("connected");
        // Initialize an empty list of resubscription promises.
        const resubscriptionPromises = [];
        // For each method we have a subscription for..
        for(const method in this.subscriptionMethods){
            // .. and for each parameter we have previously been subscribed to..
            for (const parameterJSON of this.subscriptionMethods[method].values()){
                // restore the parameters from JSON.
                const parameters = JSON.parse(parameterJSON);
                // Send a subscription request.
                resubscriptionPromises.push(this.subscribe(method, ...parameters));
            }
            // Wait for all re-subscriptions to complete.
            await Promise.all(resubscriptionPromises);
        }
        // Write a log message if there was any subscriptions to restore.
        if (resubscriptionPromises.length > 0) (0, $623a31cc1663a627$export$2e2bcd8739ae039).client(`Restored ${resubscriptionPromises.length} previous subscriptions for '${this.connection.hostIdentifier}'`);
    }
    /**
	 * Parser messages from the remote server to resolve request promises and emit subscription events.
	 *
	 * @param {RPCNotification | RPCResponse} message   the response message
	 *
	 * @throws {Error} if the message ID does not match an existing request.
	 * @ignore
	 */ response(message) {
        // If the received message is a notification, we forward it to all event listeners
        if ((0, $abcb763a48577a1e$export$280de919a0cf6928)(message)) {
            // Write a log message.
            (0, $623a31cc1663a627$export$2e2bcd8739ae039).client(`Received notification for '${message.method}' from '${this.connection.host}'`);
            // Forward the message content to all event listeners.
            this.emit("notification", message);
            // Try to update the chain height.
            this.updateChainHeightFromHeadersNotifications(message);
            // Return since it does not have an associated request resolver
            return;
        }
        // If the response ID is null we cannot use it to index our request resolvers
        if (message.id === null) // Throw an internal error, this should not happen.
        throw new Error("Internal error: Received an RPC response with ID null.");
        // Look up which request promise we should resolve this.
        const requestResolver = this.requestResolvers[message.id];
        // If we do not have a request resolver for this response message..
        if (!requestResolver) {
            // Log that a message was ignored since the request has already been rejected.
            (0, $623a31cc1663a627$export$2e2bcd8739ae039).warning(`Ignoring response #${message.id} as the request has already been rejected.`);
            // Return as this has now been fully handled.
            return;
        }
        // Remove the promise from the request list.
        delete this.requestResolvers[message.id];
        // If the message contains an error..
        if ((0, $abcb763a48577a1e$export$d73a2e87a509880)(message)) // Forward the message error to the request resolver and omit the `result` parameter.
        requestResolver(new Error(message.error.message));
        else {
            // Forward the message content to the request resolver and omit the `error` parameter
            // (by setting it to undefined).
            requestResolver(undefined, message.result);
            // Attempt to extract genesis hash from feature requests.
            this.storeGenesisHashFromFeaturesResponse(message);
        }
    }
    /**
	 * Callback function that is called when connection to the Electrum server is lost.
	 * Aborts all active requests with an error message indicating that connection was lost.
	 *
	 * @ignore
	 */ async onConnectionDisconnect() {
        // Loop over active requests
        for(const resolverId in this.requestResolvers){
            // Extract request resolver for readability
            const requestResolver = this.requestResolvers[resolverId];
            // Resolve the active request with an error indicating that the connection was lost.
            requestResolver(new Error("Connection lost"));
            // Remove the promise from the request list.
            delete this.requestResolvers[resolverId];
        }
        // Synchronize with the underlying connection status.
        this.handleConnectionStatusChanges("disconnected");
    }
    /**
	 * Stores the server provider software version field on successful version negotiation.
	 *
	 * @ignore
	 */ async storeSoftwareVersion(versionStatement) {
        // TODO: handle failed version negotiation better.
        if (versionStatement.error) // Do nothing.
        return;
        // Store the software version.
        this.software = versionStatement.software;
    }
    /**
	 * Updates the last received timestamp.
	 *
	 * @ignore
	 */ async updateLastReceivedTimestamp() {
        // Update the timestamp for when we last received data.
        this.lastReceivedTimestamp = Date.now();
    }
    /**
	 * Checks if the provided message is a response to a headers subscription,
	 * and if so updates the locally stored chain height value for this client.
	 *
	 * @ignore
	 */ async updateChainHeightFromHeadersNotifications(message) {
        // If the message is a notification for a new chain height..
        if (message.method === "blockchain.headers.subscribe") // ..also store the updated chain height locally.
        this.chainHeight = message.params[0].height;
    }
    /**
	 * Checks if the provided message is a response to a server.features request,
	 * and if so stores the genesis hash for this client locally.
	 *
	 * @ignore
	 */ async storeGenesisHashFromFeaturesResponse(message) {
        try {
            // If the message is a response to a features request..
            if (typeof message.result.genesis_hash !== "undefined") // ..store the genesis hash locally.
            this.genesisHash = message.result.genesis_hash;
        } catch (error) {
        // Do nothing.
        }
    }
    /**
	 * Helper function to synchronize state and events with the underlying connection.
	 */ async handleConnectionStatusChanges(eventName) {
        // Synchronize with the underlying connection status.
        this.status = this.connection.status;
        // Re-emit the event.
        this.emit(eventName);
    }
}
var // Export the client.
$558b46d3f899ced5$export$2e2bcd8739ae039 = $558b46d3f899ced5$var$ElectrumClient;







export {$558b46d3f899ced5$export$2e2bcd8739ae039 as ElectrumClient, $e83d2e7688025acd$export$e1f38ab2b4ebdde6 as isVersionRejected, $e83d2e7688025acd$export$9598f0c76aa41d73 as isVersionNegotiated, $d801b1f9b7fc3074$export$d048df559e6d3842 as ElectrumTransport, $d801b1f9b7fc3074$export$f019be48b3aacb1a as DefaultParameters, $db7c797e63383364$export$c4f81c6d30ca200f as ClientState, $db7c797e63383364$export$7516420eb880ab68 as ConnectionStatus};
//# sourceMappingURL=index.mjs.map
